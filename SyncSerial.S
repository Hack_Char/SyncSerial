#define __SFR_OFFSET 0
#include "avr/io.h"

.global rxByte

rxByte:
  CLI
  LDI r19, 0
  LDI r21, 0
  STS TCCR1A, r19 ; most of timer1 is configured 0
  STS OCR1AH, r24 ; timer1 high byte from input arg
  STS OCR1AL, r19
  STS TCNT1H, r19
  STS TCNT1L, r19
  LDI r23, 5
  STS TCCR1B, r23 ; set prescaler, '101' == divide by 1024
  LDI r23, 0xFF ; clear flags for timer1
  OUT TIFR1, r23
  LDI r25, 0x00 ; default status to return is '0' good
checkRx: 
  IN r22, PIND ; clock in on PD2
  IN r19, PINB ; data is in on PB4
  BST r22, 2
  BRTC clockLow ; branch if clock is low
  LDI r18, 0x01 ; clock is high - no receive
  IN r23, TIFR1 ; check if timeout
  BST r23, 1
  BRTC checkRx ; no time out, start over checking
  LDI r25, 0x01 ; timed out
  RJMP returnValue
clockLow:
  BST r18, 0 ; check if clock was high previously
  BRTC noRx  ; if clock did not go high, no receive happens
  ; Begin loading a bit
  LDI r18, 0 ; flag for clock high previously is cleared - do not clock in again this cycle
  LSR r20 ; shift right out received byte
  SBRC r19, 4 ; if rx 0, skip below
  SBR r20, 0x80 ; set MSB bit in rxByte
  INC r21 ; increment byte counter
  CPI r21, 8 ; if we received all 8 bits returnValue
  BREQ returnValue
noRx:
  IN r23, TIFR1 ; check if we timed out
  BST r23, 1
  BRTC checkRx ; did not timeout, go to the start of checks
  LDI r25, 0x01
returnValue:
  MOV r24, r20
  LDI r23, 0
  STS TCCR1B, r23 ; stop the timer1
  SEI
  RET
  