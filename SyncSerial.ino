/** Synchronous Serial Communication for the Arduino Uno
 *  ====================================================
 *  @Char@noc.social	https://gitlab.com/Hack_Char
 *
 *  This demonstrates using an Arduino Uno to send
 *  synchronous data/clock at 500Kbps.
 *  Least significant bit is sent/received first. Data is
 *  approximately mid-bit sampled at clock falling edge.
 *  Connect two Arduino Uno where TX/RX pins are swapped.
 *  Running this program will ping/pong bytes back and forth.
 *  Tip: Hold down reset on one Arduino to stop and look at the
 *       serial monitor. Use enter on serial to pause/resume.
 *  Just barely working at 500Kbps - if need to receive faster, can
 *  remove the timeout checks from the assembly. Per testing, can
 *  run at 531.9Kbps, but removing any delays beyond this causes
 *  missed bits.
 *  With no added capacitance, seeing ~2V over/undershoot.
 */

#define TX_DATA 13 // PB5
#define RX_DATA 12 // PB4
#define TX_CLOCK 3 // PD3
#define RX_CLOCK 2 // PD2 - capable of external interrupt
// Attempted to use interrupts first - but they take ~40+ cycles just to enter the ISR.

/** Returned data from assembly function call
 *  status == 0 if received entire byte
 *  status == 1 if timeout reached
 */
typedef struct rxByteValues
{
  byte recievedByte;
  byte status;
} rxByteValue;

/** Assembly function call in SyncSerial.S
 *  Return value described above
 *  Timeout is upper byte of timer1
 *   running with prescaler /1024
 *   0x40 ~= 1 second
 */
extern "C" {
  rxByteValue rxByte(byte timeout);
}

/** This will transmit one byte at 500Kbps
 * (1 bit per 2us)
 */
void txDataFast(byte txByte)
{
  byte txBits[9];
  cli(); // disable interrupts while we transmit
  for (byte i=0; i<8; i++)
  {
    // pre-populating this array with 0x20 where a bit should be sent
    txBits[i] = ((txByte >> i) & 0x01) << 5;
  }
  // Final state of data is high
  txBits[8]=0x20;
  // Since the loop takes a while, drive first data bit
  // Data bit is at PORTB 0x20
  PORTB |= txBits[0];
  PORTB &= txBits[0];
  
  // The timing here is a trade-off attempting to get a good mid-bit sample
  // while also trying to keep the clock mostly square.
  for (byte i=1; i<9; i++)
  {
    // set both delay to 6, for 500Kbps
    // set both delay to 5 for ~531.9kbps
    __builtin_avr_delay_cycles(5); // use dummy CPU cycles for delay
    PORTD &= ~0x8; // clock low - active edge

    __builtin_avr_delay_cycles(5);
    PORTB &= txBits[i];
    PORTB |= txBits[i];
    PORTD |= 0x8; // clock high
    // Driving new data bit at rising edge of the clock
  }
  sei(); // re-enable interrupts
}

void setup() {
  // Using Serial to show status on the 'Serial Monitor'
  Serial.begin(2000000, SERIAL_8N1);
  pinMode(TX_DATA, OUTPUT);
  pinMode(RX_DATA, INPUT);
  pinMode(TX_CLOCK, OUTPUT);
  pinMode(RX_CLOCK, INPUT);
  digitalWrite(TX_DATA, LOW);
  digitalWrite(TX_CLOCK, HIGH);
  Serial.println("Initialized.");
}

void loop() {
  byte txValue=0x01;
  rxByteValue rxBuffer;
  bool run=true;

  while ( true ) 
  {
    rxBuffer.recievedByte=0;
    rxBuffer.status=0;
    byte timeOut=0x80;
    
    rxBuffer = rxByte(timeOut);

    if (Serial.available() != 0)
    {
      run = ! run;
      Serial.read();
    }
    if (run)
    {
      if (rxBuffer.status == 0)
      {
        Serial.println(rxBuffer.recievedByte, HEX);
        txDataFast(rxBuffer.recievedByte + 1);
      } else
      {
        txDataFast(0x01);
      }
    }
  }
}
