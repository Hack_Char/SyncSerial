Synchronous Serial Communication for the Arduino Uno
====================================================
@Char@noc.social	https://gitlab.com/Hack_Char

2022/12/01

This project demonstrates and tests using an Arduino Uno
to send synchronous serial (data and clock) as well as 
receive at 500Kbps (one bit per 2us).

It is currently only designed to send a single byte and
recieve a single byte.

This is intended to be educational. Feel free to use 
this project however you see fit, however, please credit 
me if using any significant portion directly.
